<?php

namespace com\endpointer\lib\db\mysql;

use const com\endpointer\config\constants\EP_BIZ_ERROR;
use const com\endpointer\config\constants\EP_MYSQL_ERROR;
use const com\endpointer\lib\db\mysql\config\constants\EP_MYSQL_DB_STMT;
use const com\endpointer\lib\db\mysql\config\constants\EP_USER_ERROR;

use function com\endpointer\lib\db\getConnection;
use function com\endpointer\lib\db\getParams;
use function com\endpointer\lib\db\getProcName;
use function com\endpointer\lib\db\hasParams;
use function com\endpointer\lib\db\setConnection;

use function com\endpointer\lib\framework\config\getConfig;
use function com\endpointer\lib\framework\error\hasError;
use function com\endpointer\lib\framework\error\setError;
use function com\endpointer\lib\framework\response\setClientErrorHeader;
use function com\endpointer\lib\framework\response\setServerErrorHeader;
use function com\endpointer\lib\framework\state\getState;
use function com\endpointer\lib\framework\state\setState;

function connect() {

	$db = getConfig()['DB'];

	mysqli_report(MYSQLI_REPORT_OFF);

	setConnection(

		mysqli_connect(

			$db['host'],

			$db['user'],

			$db['pwd'],

			$db['schema']

		)

	);

	$e = mysqli_connect_errno();

	if (

		$e	=== 0

	) {
	} else {

		setError("MYSQL $e");

		setServerErrorHeader();

		return;
	}

	mysqli_set_charset(

		getConnection(),

		$db['charset']

	);

	mysqli_stmt_execute(

		mysqli_prepare(

			getConnection(),

			"SET time_zone	=	\"{$db['timezone']}\""

		)
	);

	mysqli_stmt_execute(

		mysqli_prepare(

			getConnection(),

			"SET sql_mode	=	strict_mode"

		)
	);
}

function disconnect() {

	if (isConnected()) {

		mysqli_close(getConnection());
	}
}

function isConnected() {

	return (

		(getConnection() !== false)

		&& mysqli_ping(getConnection()));
}

function getDataTypes() {

	$dataTypes = '';

	if (hasParams()) {

		foreach ((array)getParams()

			as $v) {

			if (is_int($v)) {

				$dataTypes .= 'i';
			} elseif (is_double($v)) {

				$dataTypes .= 'd';
			} else {

				$dataTypes .= 's';
			}
		}

		return $dataTypes;
	}
}

function getQuery() {

	$pn = getProcName();

	$q    =    "CALL $pn (";

	if (

		hasParams()

	) {

		for (

			$i = 0;

			$i < (count(getParams()) - 1);

			$i++

		) {

			$q    .=    '? ,';
		}

		$q    .=    '?';
	}

	$q    .=    ')';

	return $q;
}

function prepareStatement() {

	setState(EP_MYSQL_DB_STMT, mysqli_prepare(

		getConnection(),

		getQuery()

	));
}

function getStatement() {

	return getState(EP_MYSQL_DB_STMT);
}

function getResult() {

	if (hasError()) {

		return;
	}

	if (

		hasParams()

	) {

		$p = array_values((array)getParams());

		mysqli_stmt_bind_param(

			getStatement(),

			getDataTypes(),

			...$p

		);
	}

	if (

		mysqli_stmt_execute(getStatement())

	) {
	} else {

		if (mysqli_errno(getConnection()) === EP_USER_ERROR) {

			$c = EP_BIZ_ERROR;

			$e = mysqli_error(getConnection());

			setError("$c $e");

			setClientErrorHeader();

			return;
		}

		$c = EP_MYSQL_ERROR;
		$e = mysqli_errno(getConnection());

		setError("$c $e");

		setServerErrorHeader();
	}

	return mysqli_stmt_get_result(getStatement());
}

function releaseStatement() {

	mysqli_stmt_close(getStatement());
}
