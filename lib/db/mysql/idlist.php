<?php

namespace com\endpointer\lib\db\mysql;

use function com\endpointer\lib\db\mysql\getResult;
use function com\endpointer\lib\db\setIdList;
use function com\endpointer\lib\framework\error\hasError;

function idlist() {

	prepareStatement();

	$res	= getResult();

	if (hasError()) {

		return;
	}

	$idList = [];

	while (

		($row	= mysqli_fetch_array(

			$res,

			MYSQLI_ASSOC

		))	!== null

	) {

		$idList[]	=	$row['id'];
	}

	setIdList($idList);

	mysqli_free_result($res);

	releaseStatement();
}
