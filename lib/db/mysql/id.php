<?php

namespace com\endpointer\lib\db\mysql;

use function com\endpointer\frontend\lib\framework\state\s;
use function com\endpointer\frontend\lib\framework\state\sa;

function idl() {

	/*

	[
		'n'=>'',//procedure name
		't'=>''//parameters data types
		'p'=>[]// parameters
	]

	*/

	$p = s('EP_DB_PARAMS');

	$q	=	"CALL {$p['procname']} (";

	for (

		$i = 0;

		$i < (count($p['params']) - 1);

		$i++

	) {

		$q	.=	'? ,';
	}

	$q	.=	'?)';

	$stmt = mysqli_prepare(

		s('EP_DB_CONNONN'),

		$q

	);

	mysqli_stmt_bind_param(

		$stmt,

		$p['datatypes'],

		...$p['params']

	);

	if (

		mysqli_stmt_execute($stmt)

	) {
	} else {

		sa('EP_ERROR', mysqli_error(s('EP_DB_CONN')));

		mysqli_stmt_close($stmt);

		return;
	}

	$res	= mysqli_stmt_get_result($stmt);

	$row	= mysqli_fetch_array(

		$res,

		MYSQLI_ASSOC

	);

	if (

		$row	===	NULL

	) {

		sa('EP_ERROR', 'RECNOTFOUND');
	} else {

		sa('EP_DB_ID', $row['id']);
	}

	mysqli_free_result(

		$res

	);

	mysqli_stmt_close(

		$stmt

	);
}
