<?php

namespace com\endpointer\lib\db\mysql;

use function com\endpointer\lib\db\mysql\getResult;
use function com\endpointer\lib\db\setLastId;
use function com\endpointer\lib\framework\error\hasError;

function create() {

	prepareStatement();

	$res	= getResult();

	if (hasError()) {

		return;
	}

	$row	= mysqli_fetch_array(

		$res,

		MYSQLI_ASSOC

	);

	setLastId($row['lastId']);

	mysqli_free_result($res);

	releaseStatement();
}
