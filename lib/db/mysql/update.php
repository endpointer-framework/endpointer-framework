<?php

namespace com\endpointer\lib\db\mysql;

use function com\endpointer\lib\db\mysql\getResult;
use function com\endpointer\lib\db\setRowCount;
use function com\endpointer\lib\framework\error\hasError;

function update() {

	prepareStatement();

	$res	= getResult();

	if (hasError()) {

		return;
	}

	$row	= mysqli_fetch_array(

		$res,

		MYSQLI_ASSOC

	);

	setRowCount($row['rowCount']);

	mysqli_free_result($res);

	releaseStatement();
}
