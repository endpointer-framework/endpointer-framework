<?php

namespace com\endpointer\lib\db\mysql;

use function com\endpointer\lib\db\mysql\getResult;
use function com\endpointer\lib\db\setRec;

use function com\endpointer\lib\framework\error\hasError;

function read() {

	prepareStatement();

	$res	= getResult();

	if (hasError()) {

		return;
	}

	$row	= (mysqli_fetch_array(

		$res,

		MYSQLI_ASSOC

	));

	setRec($row);

	mysqli_free_result($res);

	releaseStatement();

}
