<?php

namespace com\endpointer\lib\db\config\constants;

const EP_DB_CONN = 'EP_DB_CONN';

const EP_DB_PROC = 'EP_DB_PROC';
const EP_DB_PROC_NAME = 'EP_DB_PROC_NAME';
const EP_DB_PROC_PARAMS = 'EP_DB_PROC_PARAMS';

const EP_DB_IDLIST = 'EP_DB_IDLIST';
const EP_DB_ID = 'EP_DB_ID';
const EP_DB_RECORD = 'EP_DB_RECORD';
const EP_DB_LASTID = 'EP_DB_LASTID';
const EP_DB_ROWCOUNT = 'EP_DB_ROWCOUNT';
