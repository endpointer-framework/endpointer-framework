<?php

namespace com\endpointer\lib\db;

use const com\endpointer\lib\db\config\constants\EP_DB_CONN;
use const com\endpointer\lib\db\config\constants\EP_DB_ID;
use const com\endpointer\lib\db\config\constants\EP_DB_IDLIST;
use const com\endpointer\lib\db\config\constants\EP_DB_LASTID;
use const com\endpointer\lib\db\config\constants\EP_DB_PROC_NAME;
use const com\endpointer\lib\db\config\constants\EP_DB_PROC_PARAMS;
use const com\endpointer\lib\db\config\constants\EP_DB_RECORD;
use const com\endpointer\lib\db\config\constants\EP_DB_ROWCOUNT;

use function com\endpointer\getRoot;

use function com\endpointer\lib\framework\config\getConfig;
use function com\endpointer\lib\framework\state\getState;
use function com\endpointer\lib\framework\state\hasState;
use function com\endpointer\lib\framework\state\setState;

function connect() {

    $r = getRoot();

    $t = getDbType();

    require_once "$r/lib/db/_config/constants.php";
    require_once "$r/lib/db/_config/general.php";

    require_once "$r/lib/db/$t/_config/constants.php";
    require_once "$r/lib/db/$t/_config/general.php";

    require_once "$r/lib/db/$t/db.php";
    require_once "$r/lib/db/$t/idlist.php";
    require_once "$r/lib/db/$t/create.php";
    require_once "$r/lib/db/$t/read.php";
    require_once "$r/lib/db/$t/update.php";
    require_once "$r/lib/db/$t/delete.php";

    $connect = "com\\endpointer\lib\db\\$t\connect";

    $connect();
}

function disconnect() {

    $t = getDbType();

    $disconnect = "com\\endpointer\lib\db\\$t\disconnect";

    $disconnect();
}

function getDbType() {

    $cfg = getConfig();

    return $cfg['DB']['type'];
}

function setProcName($v) {

    setState(EP_DB_PROC_NAME, $v);
}

function setParams($v) {

    setState(EP_DB_PROC_PARAMS, $v);
}

function setIdList($v) {

    setState(EP_DB_IDLIST, $v);
}

function setId($v) {

    setState(EP_DB_ID, $v);
}

function setRec($v) {

    setState(EP_DB_RECORD, $v);
}

function setLastId($v) {

    setState(EP_DB_LASTID, $v);
}

function setRowCount($v) {

    setState(EP_DB_ROWCOUNT, $v);
}

function getProcName() {

    return getState(EP_DB_PROC_NAME);
}

function getParams() {

    return getState(EP_DB_PROC_PARAMS);
}

function hasParams() {

    return hasState(EP_DB_PROC_PARAMS);
}

function getIdList() {

    return getState(EP_DB_IDLIST);
}

function getId() {

    return getState(EP_DB_ID);
}

function getRec() {

    return getState(EP_DB_RECORD);
}

function getLastId() {

    return getState(EP_DB_LASTID);
}

function getRowCount() {

    return getState(EP_DB_ROWCOUNT);
}

function setConnection($v) {

    setState(EP_DB_CONN, $v);
}

function getConnection() {

    return getState(EP_DB_CONN);
}

function idlist() {

    $t = getDbType();

    $f = "com\\endpointer\lib\db\\$t\idlist";

    $f();
}

function create() {

    $t = getDbType();

    $f = "com\\endpointer\lib\db\\$t\create";

    $f();
    
}

function read() {

    $t = getDbType();

    $f = "com\\endpointer\lib\db\\$t\\read";

    $f();
    
}

function update() {

    $t = getDbType();

    $f = "com\\endpointer\lib\db\\$t\update";

    $f();
    
}

function delete() {

    $t = getDbType();

    $f = "com\\endpointer\lib\db\\$t\delete";

    $f();
    
}
