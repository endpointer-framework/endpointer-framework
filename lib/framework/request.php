<?php

namespace com\endpointer\lib\framework\request;

use function com\endpointer\lib\framework\json\setJson;
use function com\endpointer\lib\framework\json\getJson;
use function com\endpointer\lib\framework\json\jsonDecode;

use function com\endpointer\lib\framework\error\hasError;
use function com\endpointer\lib\framework\error\setError;
use function com\endpointer\lib\framework\response\setClientErrorHeader;
use function com\endpointer\lib\framework\state\setState;
use function com\endpointer\lib\framework\state\getState;

use const com\endpointer\config\constants\EP_REQ_URLDATA;
use const com\endpointer\config\constants\EP_REQ_BODYDATA;

use const com\endpointer\config\constants\EP_VERSION;
use const com\endpointer\config\constants\EP_RESTYPE;
use const com\endpointer\config\constants\EP_EP;
use const com\endpointer\config\constants\EP_REQ_VERB;
use const com\endpointer\config\constants\EP_REST_ERROR;
use const com\endpointer\config\constants\REST_NOID;
use const com\endpointer\config\constants\REST_NOTIMPLEMENTED;

function prepareRequest() {

    $s = $_SERVER;
    $g = $_GET;

    setState(EP_REQ_VERB, $s['REQUEST_METHOD']);

    setState(EP_VERSION, $g[EP_VERSION]);
    setState(EP_RESTYPE, $g[EP_RESTYPE]);
    setState(EP_EP, $g[EP_EP]);

    setState(EP_REQ_URLDATA, $g);

    $e = EP_REST_ERROR;

    if (getVerb() === 'GET') {
    } elseif (getVerb() === 'POST') {

        setBodyData();
    } elseif (getVerb() === 'PUT') {
        if (hasUrlField('id')) {
            setBodyData();
        } else {

            $c = REST_NOID;

            setError("$e $c");

            setClientErrorHeader();
        }
    } elseif (getVerb() === 'DELETE') {

        if (hasUrlField('id')) {
        } else {

            $c = REST_NOID;

            setError("$e $c");

            setClientErrorHeader();
        }
    } else {

        $c = REST_NOTIMPLEMENTED;

        setError("$e $c");


        setClientErrorHeader();
    }
}

function setBodyData() {

    setJson(file_get_contents('php://input'));

    jsonDecode();

    if (hasError()) {

        setClientErrorHeader();
    } else {

        setState(EP_REQ_BODYDATA, getJson());
    }
}

function getApiVersion() {

    global $EP_STATE;

    return $EP_STATE[EP_VERSION];
}

function getResourceType() {

    return getState(EP_RESTYPE);
}

function getEndpoint() {

    return getState(EP_EP);
}

function getVerb() {

    return getState(EP_REQ_VERB);
}

function getUrlData() {

    return getState(EP_REQ_URLDATA);
}

function getUrlField($k) {

    return getUrlData()[$k];
}

function hasUrlField($k) {

    return array_key_exists($k, getUrlData());
}

function getBodyData() {

    return getState(EP_REQ_BODYDATA);
}
