<?php

namespace com\endpointer\lib\framework\state;

use const com\endpointer\config\constants\EP_LOCAL_STATE;

$EP_STATE = [EP_LOCAL_STATE => []];

function setState($k, $v) {

    global $EP_STATE;

    $EP_STATE[$k] = $v;
}

function unsetState($k) {

    global $EP_STATE;

    $EP_STATE[$k] = null;
}

function getState($k) {

    global $EP_STATE;

    return $EP_STATE[$k];
}

function hasState($k) {

    global $EP_STATE;

    return isset($EP_STATE[$k]);
}

function &getStateRef($k) {

    global $EP_STATE;

    return $EP_STATE[$k];
}

function isStateEqual($k, $v) {

    global $EP_STATE;

    return ($EP_STATE[$k] === $v);
}

function dumpState($print = true) {

    global $EP_STATE;

    $print ? print_r($EP_STATE) : null;

    return $EP_STATE;
}

function setLocalState($k, $v) {

    getStateRef(EP_LOCAL_STATE)[$k] = $v;
}

function unsetLocalState($k) {

    getStateRef(EP_LOCAL_STATE)[$k] = null;
}

function getLocalState($k) {

    return getState(EP_LOCAL_STATE)[$k];
}

function hasLocalState($k) {

    return isset(getState(EP_LOCAL_STATE)[$k]);
}

function &getLocalStateRef($k) {

    return getStateRef(EP_LOCAL_STATE);
}

function isLocalStateEqual($k, $v) {

    return (getState(EP_LOCAL_STATE)[$k] === $v);
}

function dumpLocalState() {

    print_r(getState(EP_LOCAL_STATE));

}
