<?php

namespace com\endpointer\lib\framework\config;

use const com\endpointer\config\constants\EP_CONFIG;

use function com\endpointer\lib\framework\state\getState;
use function com\endpointer\lib\framework\state\setState;

function setConfig($v) {

    setState(EP_CONFIG, $v);
}

function getConfig() {

    return getState(EP_CONFIG);
}

function dumpConfig($print = true) {

    $print ? print_r(getConfig()) : null;
}
