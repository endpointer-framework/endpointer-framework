<?php

namespace com\endpointer\lib\framework\error;

use function com\endpointer\lib\framework\response\setServerErrorHeader;
use function com\endpointer\lib\framework\state\getState;
use function com\endpointer\lib\framework\state\setState;

use const com\endpointer\config\constants\EP_ERROR;
use const com\endpointer\config\constants\EP_PHP_ERROR;

function hasError() {

    return (getState(EP_ERROR) !== '');
}

function dumpError($p = false) {

    if (hasError()) {

        var_dump(getError());

        return;
    } elseif ($p) {

        echo "\nNo errors.";
    }
}

function clearError() {

    setError('');
}

function setError($v) {

    setState(EP_ERROR, $v);
}

function getError() {

    return getState(EP_ERROR);
}

function sysError($errno, $errmsg) {

    $c = EP_PHP_ERROR;

    setError("$c $errno $errmsg");

    setServerErrorHeader();

    debug_print_backtrace();
}
