<?php

namespace com\endpointer\lib\framework\curl;

use const com\endpointer\config\constants\EP_CURL_ERROR;
use const com\endpointer\config\constants\EP_URL;

use function com\endpointer\lib\framework\error\setError;

use function com\endpointer\lib\framework\json\jsonDecode;
use function com\endpointer\lib\framework\json\setJson;

use function com\endpointer\lib\framework\state\setState;
use function com\endpointer\lib\framework\state\getState;

function get() {

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, getUrl());

    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt($ch, CURLOPT_FAILONERROR, true);

    $json = curl_exec($ch);

    if (curl_errno($ch) === CURLE_OK) {

        setJson($json);

        jsonDecode();
    } else {

        setCurlError($ch);
    }

    curl_close($ch);
}

function setUrl($v) {

    setState(EP_URL, $v);
}

function getUrl() {

    return getState(EP_URL);
}

function setCurlError($ch) {

    $c = EP_CURL_ERROR;
    $e = curl_errno($ch);

    setError("$c $e");

    http_response_code(500);
}