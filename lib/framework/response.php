<?php

namespace com\endpointer\lib\framework\response;

use function com\endpointer\lib\framework\error\getError;

use const com\endpointer\config\constants\EP_HEADER_ERROR;

function setClientErrorHeader() {

    $h = EP_HEADER_ERROR;
    $e = getError();

    http_response_code(400);

    header("$h: $e");
}

function setServerErrorHeader() {

    $h = EP_HEADER_ERROR;
    $e = getError();

    http_response_code(500);

    header("$h: $e");
}
