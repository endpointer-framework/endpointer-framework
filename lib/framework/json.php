<?php

namespace com\endpointer\lib\framework\json;

use function com\endpointer\lib\framework\error\setError;
use function com\endpointer\lib\framework\state\setState;
use function com\endpointer\lib\framework\state\getState;

use const com\endpointer\config\constants\EP_JSON;
use const com\endpointer\config\constants\EP_JSON_ERROR;

function jsonDecode() {

    $json = json_decode(getJson(), true);

    if (json_last_error() === JSON_ERROR_NONE) {

        setJson($json);

        return;
    }

    setJsonError();
}

function jsonEncode() {

    $json = json_encode(getJson());

    if (json_last_error() === JSON_ERROR_NONE) {

        setJson($json);

        return;
    }

    setJsonError();
}

function setJson($v) {

    setState(EP_JSON, $v);
}

function getJson() {

    return getState(EP_JSON);
}

function setJsonError() {

    $c = EP_JSON_ERROR;
    $e = json_last_error();

    setError("$c $e");
}
