<?php

namespace com\endpointer\v0\services\config\general;

use const com\endpointer\config\general\CFG as c;

const CFG = [

    'DB' => [

        'type' => c['DB']['type'],
        'host' => c['DB']['host'],
        'schema' => c['DB']['schema'],
        'user' => c['DB']['user'],
        'pwd' => c['DB']['pwd'],
        'charset' => c['DB']['charset'],
        'timezone' => c['DB']['timezone']

    ]

];
