<?php

namespace com\endpointer\v0\services\status;

use const com\endpointer\v0\services\status\config\g\CFG;

use function com\endpointer\lib\framework\config\setConfig;
use function com\endpointer\lib\framework\request\getVerb;

use function com\endpointer\lib\framework\error\hasError;
use function com\endpointer\v0\services\serverdata\state\setStatus;
use function com\endpointer\v0\services\serverdata\view\sendStatus;

function controller() {

    prepare();

    if (hasError()) {

        release();

        return;
    }

    switch (getVerb()) {

        case 'GET':

            get();

            break;
    }

    release();
}

function prepare() {

    setConfig(CFG);

    \com\endpointer\lib\db\connect();
}

function release() {

    \com\endpointer\lib\db\disconnect();
}

function get() {

    setStatus('Endpointer Framework is on!');

    sendStatus();
}
