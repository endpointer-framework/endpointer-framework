<?php

namespace com\endpointer\v1\services\serverdata\model;

use function com\endpointer\v1\services\serverdata\state\setDate;
use function com\endpointer\v1\services\serverdata\state\setTime;

function getServerData() {

    setDate(date('Y-m-d'));
    setTime(date('H:i:s'));
}
