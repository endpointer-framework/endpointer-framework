<?php

namespace com\endpointer\v0\services\serverdata\view;

use function com\endpointer\lib\framework\json\getJson;
use function com\endpointer\lib\framework\json\jsonEncode;
use function com\endpointer\lib\framework\json\setJson;
use function com\endpointer\v0\services\serverdata\state\getStatus;

function sendStatus() {

    setJson([

        'status' => getStatus()

    ]);

    jsonEncode();

    echo getJson();
}
