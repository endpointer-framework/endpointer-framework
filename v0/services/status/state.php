<?php

namespace com\endpointer\v0\services\serverdata\state;

use const com\endpointer\v0\services\serverdata\config\constants\GENERAL_STATUS;

use function com\endpointer\lib\framework\state\setLocalState;
use function com\endpointer\lib\framework\state\getLocalState;

function setStatus($v) {

    setLocalState(GENERAL_STATUS, $v);
}

function getStatus() {

    return getLocalState(GENERAL_STATUS);
}
