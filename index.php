<?php

namespace com\endpointer;

require_once "_config/general.php";
require_once "_config/constants.php";

require_once "lib/framework/state.php";
require_once "lib/framework/error.php";
require_once "lib/framework/json.php";
require_once "lib/framework/curl.php";
require_once "lib/framework/config.php";
require_once "lib/framework/request.php";
require_once "lib/framework/response.php";

require_once "lib/db/db.php";

use const com\endpointer\config\constants\EP_ROOT;

use function com\endpointer\lib\framework\request\prepareRequest;
use function com\endpointer\lib\framework\request\getApiVersion;
use function com\endpointer\lib\framework\request\getResourceType;
use function com\endpointer\lib\framework\request\getEndpoint;

use function com\endpointer\lib\framework\error\clearError;
use function com\endpointer\lib\framework\error\getError;
use function com\endpointer\lib\framework\error\hasError;
use function com\endpointer\lib\framework\state\getState;
use function com\endpointer\lib\framework\state\setState;

function controller() {

    prepare();

    if (hasError()) {

        $e = getError();

        release();

        return;
    }

    $r = getRoot();

    $v = getApiVersion();
    $t = getResourceType();
    $e = getEndpoint();

    require_once "$r/$v/$t/_config/general.php";
    require_once "$r/$v/$t/_config/constants.php";

    require_once "$r/$v/$t/$e/_config/general.php";
    require_once "$r/$v/$t/$e/_config/constants.php";

    require_once "$r/$v/$t/$e/controller.php";
    require_once "$r/$v/$t/$e/model.php";
    require_once "$r/$v/$t/$e/mock.php";
    require_once "$r/$v/$t/$e/state.php";
    require_once "$r/$v/$t/$e/db.php";
    require_once "$r/$v/$t/$e/rest.php";
    require_once "$r/$v/$t/$e/view.php";

    $endpointController = "com\\endpointer\\$v\\$t\\$e\controller";

    $endpointController();

    if (hasError()) {

        $e = getError();
    }

    release();
}

function prepare() {

    $r = str_replace('\\', '/', __DIR__);

    setState(EP_ROOT, $r);

    clearError();

    prepareRequest();
}

function release() {
}

function getRoot() {

    return getState(EP_ROOT);
}

ini_set('display_errors', 1);

error_reporting(E_ALL);

set_error_handler('com\endpointer\lib\framework\error\sysError');

controller();
