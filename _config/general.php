<?php

namespace com\endpointer\config\general;

const CFG = [

    'DB' => [

        'type' => 'mysql',
        'host' => 'localhost',
        'schema' => 'test',
        'user' => 'root',
        'pwd' => 'root',
        'charset' => 'utf8mb4',
        'timezone' => '-3:00'

    ],

    'URL' => [

        'host' => 'local.endpointer.com:8000',

        'uri' => '/api/v1/services/serverdata'

    ]

];
