Endpointer Framework is a PHP Framework to develop REST Endpoints with Procedural Programming.

It's free to use by anybody for any legal purpose.

Endpointer Framework is maintained by Endpointer ESP (Endpoint Service Provider) Ltda.

Documentation

https://gitlab.com/endpointer1/endpointer-framework/-/wikis/home

Contact

http://forum.endpointer.com

Site

http://endpointer.com